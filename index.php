<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 3</title>
</head>
<body>
    
    <?php

        $palabra_buscar = 'molino';
        $total_palabras = 0;
        $fd = fopen('quijote.txt','r');

        while (($linea = fgets($fd)) !== false){
            $palabras_en_linea = substr_count(strtolower($linea),strtolower($palabra_buscar));
            $total_palabras += $palabras_en_linea;
        }

        fclose($fd);
 
        echo "La palabra ".$palabra_buscar." aparece ".$total_palabras." veces en el fichero";
    ?>

</body>
</html>
